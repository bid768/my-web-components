// (function() {
class PopInfoBox extends HTMLElement {
  constructor() {
    super();

    const shadowDom = this.attachShadow({ mode: "open" });

    const wrapperEle = document.createElement("span");
    wrapperEle.setAttribute("class", "wrapper");

    const iconEle = document.createElement("span");
    iconEle.setAttribute("class", "icon");
    iconEle.setAttribute("tabindex", 0);

    const infoEle = document.createElement("span");
    infoEle.setAttribute("class", "info");

    const text = this.getAttribute("data-text");
    infoEle.textContent = text;

    let imgUrl;

    if (this.hasAttribute("img")) {
      imgUrl = this.getAttribute("img");
    } else {
      imgUrl = "img/default.png";
    }

    const imgEle = document.createElement("img");
    imgEle.src = imgUrl;
    iconEle.appendChild(imgEle);

    const styleEle = document.createElement("style");
    styleEle.textContent = `.wrapper {
      position: relative;
    }
    .info {
      position: absolute;
      top: 20px;
      left: 10px;
      z-index: 3;
      display: inline-block;
      opacity: 0;
      font-size: 0.8rem;
      padding: 10px;
      width: 200px;
      background: white;
      border: 1px solid black;
      border-radius: 10px;
      transition: 0.6s all;
    }
    img {
      width: 1.2rem;
    }
    .icon:hover + .info, icon:focus + .info {
      opacity: 1;
    }
    `;
    shadowDom.appendChild(styleEle);

    shadowDom.appendChild(wrapperEle);
    wrapperEle.appendChild(iconEle);
    wrapperEle.appendChild(infoEle);
  }
}
// 让浏览器知道自定义元素
customElements.define("pop-info-box", PopInfoBox);
// })();
