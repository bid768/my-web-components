// (function() {
  class CustomEle extends HTMLElement {
    constructor() {
      super();

      const shadowRoot = this.attachShadow({ mode: "open" });
      const divEle = document.createElement("div");
      divEle.textContent = this.getAttribute('text');
      shadowRoot.appendChild(divEle);
    }
  }
  // 让浏览器知道自定义元素
  customElements.define("custom-ele", CustomEle);
// })();
