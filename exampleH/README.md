## 一、定义类

1、shadow

const shadow = this.attachShadow({ mode: "open" });

this.shadowRoot

this.attributes

## 二、事件绑定

connectedCallback() {}

## 三、让类起作用

customElements.define("add-del-item", AddDelItem);

## 四、效果图

![增删列表项](./addDeleteItem.gif)