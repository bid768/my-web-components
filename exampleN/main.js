class MyLayout extends HTMLElement {
    constructor() {
        super();

        const shadowDom = this.attachShadow({mode: 'open'});
        const tplEle = document.createElement('template');
        
        tplEle.innerHTML = `<div class="sbBox"><header class="sbTitle"><slot name="J_head"></slot></header>
        <main class="sbMain"><slot name="J_main"></slot></main>
        <aside class="sbLeft"><slot name="J_left"></slot></aside>
        <aside class="sbRight"><slot name="J_right"></slot></aside>
        <footer class="sbFoot"><slot name="J_foot"></slot></footer></div>`
        const styleEle = document.createElement('style');
        styleEle.textContent = `
        *{
            margin: 0;
            padding: 0;
        }
        .sbTitle {
            background: red;
          grid-area: tmodule;
          height: 100%;
        }
        .sbFoot {
            background: green;
          grid-area: footer;
        }
        .sbMain {
            background: blue;
          grid-area: main;
        }
        .sbLeft {
            background: pink;
          grid-area: lmodule;
        }
        .sbRight {
            background: yellow;
          grid-area: rmodule;
        }
        .sbBox {
          display: grid;
          grid-template-areas:"tmodule tmodule tmodule" 
                              "lmodule main rmodule" 
                              "footer footer footer";
          grid-template-columns: 150px 1fr 150px;
          grid-template-rows: 100px 1fr 30px;
          min-height: 100vh;
        }
        @media screen and (max-width: 600px) {
          .sbBox {
            grid-template-areas: "tmodule" 
                                 "lmodule" 
                                 "main" 
                                 "rmodule" 
                                 "footer";
            grid-template-columns: 100%;
            grid-template-rows: 100px 50px 1fr 50px 30px;
          }
        }
        `;
        shadowDom.appendChild(styleEle);
        shadowDom.appendChild(tplEle.content.cloneNode(true));
    }
}
    
customElements.define('my-layout', MyLayout)
    
   